#include <stdlib.h>
#include "lib/bmp_io.h"
#include "lib/bmp_transform.h"

int main() {
    char* filename = "/home/lanolin/DataDrive/Projects/CLion/llp-lab-5(6)/transform/dig10k_penguin.bmp";
    BMP_File* bmpFile = readBMP(filename);
    if(bmpFile == NULL) {
        exit(1);
    }

    char* newFilename_1 = "/home/lanolin/DataDrive/Projects/CLion/llp-lab-5(6)/transform/clone_1.bmp";
    BMP_File* newBMP_1 = rotate(bmpFile, 90);
    writeBMP(newBMP_1, newFilename_1);
    free(newBMP_1);

    char* newFilename_2 = "/home/lanolin/DataDrive/Projects/CLion/llp-lab-5(6)/transform/clone_2.bmp";
    BMP_File* newBMP_2 = rotate(bmpFile, 180);
    writeBMP(newBMP_2, newFilename_2);
    free(newBMP_2);

    char* newFilename_3 = "/home/lanolin/DataDrive/Projects/CLion/llp-lab-5(6)/transform/clone_3.bmp";
    BMP_File* newBMP_3 = rotate(bmpFile, 270);
    writeBMP(newBMP_3, newFilename_3);
    free(newBMP_3);

    free(bmpFile);
    return 0;
}